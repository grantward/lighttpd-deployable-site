#!/usr/bin/env python3

import bscgi
import sitespecific as site

def index():
    """Home/index page of site."""
    return bscgi.add_headers(site.render('home.html'))


if __name__ == '__main__':
    print(index())
