//Provides page color control
//Requires cookies.js

const W3_COLORS = [
  "black",
  "white",
];

window.onload = loadThemeCookie();

function loadThemeCookie() {
  //Set the theme color based on saved cookie
  var theme = getCookieValue("color-theme");
  if (theme) {
    setColorTheme(theme);
  }
}

function toggleDarkMode() {
  //Toggle between light mode and dark mode
  var large = document.getElementById("theme-icon-large");
  var newTheme = "black";
  if (large.innerHTML === "light_mode") {
    //Currently dark, go light
    newTheme = "white";
  }
  setColorTheme(newTheme);
}

function setColorTheme(color) {
  //Toggle between light mode and dark mode
  var large = document.getElementById("theme-icon-large");
  var small = document.getElementById("theme-icon-small");
  const DARK = ["black"];
  const LIGHT = ["white"];
  var newIcon = "dark_mode";
  if (DARK.indexOf(color) !== -1) {
    newIcon = "light_mode";
  }
  setBg(color);
  setCookie("color-theme", color, 365);
  large.innerHTML = newIcon;
  small.innerHTML = newIcon;
}

function elementChangeW3Color(element, newColor) {
  //Remove any other colors from an element's class, add the new color.
  function removeW3Color(color) {
    var w3Color = "w3-" + color;
    element.className = element.className.replace(w3Color, "");
  }
  W3_COLORS.forEach(removeW3Color);
  element.className += " w3-" + newColor;
  element.className = element.className.replace("  ", " ");
}

function setBg(color) {
  //Change the background w3 color of selected body elements
  if (W3_COLORS.indexOf(color) !== -1) {
    var classname = "w3-" + color;
    var bodyBlock = document.getElementById("entire-body");
    elementChangeW3Color(bodyBlock, color);
  }
}
