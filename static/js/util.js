//Provides utilities for other Javascript

function hideElementById(id) {
  var elem = document.getElementById(id);
  if (elem) {
    elem.className = elem.className.replace(/w3-show/gi, "");
    elem.className = elem.className.replace(/  /g, " ");
  }
}

function unhideElementById(id) {
  //Prevent w3-shows from accumulating
  hideElementById(id);
  var elem = document.getElementById(id);
  if (elem) {
    elem.className += " w3-show";
  }
}

function toggleHideElementById(id) {
  var elem = document.getElementById(id);
  if (elem) {
    if (elem.className.indexOf("w3-show") !== -1) {
      //Currently shown, hide
      hideElementById(id);
    } else {
      //Currently hidden, show
      unhideElementById(id);
    }
  }
}

function getChildByText(element, text) {
  var children = element.childNodes;
  for (var i=0; i<children.length; i++) {
    if (children[i].innerHTML === text) {
      return children[i];
    }
  }
  return null;
}
