//Handles generation of HTML elements

function genResultSelectorBox(name, header, filterBox) {
  //Generate an HTML selector box w/ header and filter box
  const CLASS_NAME = "w3-card-4 w3-margin";

  var boxId = "result-" + name + "-selector-box";
  var box = document.createElement("div");
  box.id = boxId;
  box.className = CLASS_NAME;
  box.appendChild(header);
  box.appendChild(filterBox);
  return box;
}

function genResultSelectorHeader(name) {
  //Generate a header to label and open/close filter box
  var header = document.createElement("header");
  var textElement = document.createElement("h2");
  var text = document.createTextNode(name);
  var openButton = document.createElement("button");
  var openButtonIcon = document.createElement("i");
  var openButtonIconText = document.createTextNode("menu");

  //HTML classes
  header.className = "w3-bar w3-blue";
  textElement.className = "w3-bar-item w3-center";
  openButton.className = "w3-btn w3-bar-item w3-right";
  openButtonIcon.className = "material-icons";

  //Other attributes
  openButton.onclick = function() { toggleFilterBoxByName(name); };

  //Parent the elements
  textElement.appendChild(text);
  openButtonIcon.appendChild(openButtonIconText);
  openButton.appendChild(openButtonIcon);
  header.appendChild(textElement);
  header.appendChild(openButton);
  return header;
}

function genResultFilterBox(name) {
  //Generate the hideable box containing all filters for this selector box
  var box = document.createElement("div");

  //Add HTML attributes
  box.id = "result-" + name + "-selector-filter-box";
  box.className = "w3-bar w3-hide";

  return box;
}

function toggleFilterBoxByName(name) {
  //Get hide/unhide filter box by category (name)
  filterBoxId = "result-" + name + "-selector-filter-box";
  toggleHideElementById(filterBoxId);
}
