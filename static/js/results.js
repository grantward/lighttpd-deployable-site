//Handles display and screening of results

const RESULTS_PER_PAGE = 5;
const FILTER_BUTTON_OFF_CLASS = (
  "btn-off w3-bar-item w3-mobile w3-center" +
  "w3-btn w3-white w3-border w3-border-blue");
const FILTER_BUTTON_ON_CLASS = (
  "btn-on w3-bar-item w3-mobile w3-center w3-btn w3-blue w3-border");

const RESULT_ATTRIBUTES = {
  "category": ["consumer", "commercial"],
  "size": ["small", "medium", "large"],
}

//Select attributes for filtering
var resultSelectedAttributes = {};

//Array of results currently found
var filteredResults;
//Current page number
var currentPage;

window.onload = resultsPageLoad();

function resultsPageLoad() {
  //Prepares results array on page load
  resultsResetFilters();
  resultsPopulateFilterRow("category");
  resultsPopulateFilterRow("size");
}

function resultsClickFilterButton(rowId, btnId, name) {
  var element = document.getElementById(btnId);
  var rowElement = document.getElementById(rowId);
  var anyButton = getChildByText(rowElement, "any");
  if (name === "any") {
    //Unset all filters, set 'any' button
    rowElement.childNodes.forEach(resultsFilterButtonOff);
    resultsFilterButtonOn(element);
  } else {
    //Toggle this specific filter
    if (element.className.indexOf("result-btn-off") !== -1) {
      //Button off, toggle on
      resultsFilterButtonOn(element);
      //Unset any button
      resultsFilterButtonOff(anyButton);
    } else {
      //Button on, toggle off
      resultsFilterButtonOff(element);
      //TODO: if nothing set, reset to any
    }
  }
}

function resultsFilterButtonOn(button) {
  button.className = FILTER_BUTTON_ON_CLASS;
}

function resultsFilterButtonOff(button) {
  button.className = FILTER_BUTTON_OFF_CLASS;
}

function resultsPopulateFilterRow(filterName) {
  function addButton(text) {
    var btn = document.createElement("button");
    var textNode = document.createTextNode(text);
    var btnId = "result-" + filterName + "-" + text + "-button";
    btn.id = btnId;
    btn.className = FILTER_BUTTON_OFF_CLASS;
    btn.onclick = function() {
      resultsClickFilterButton(newFilterBox.id, btnId, text)
    };
    btn.appendChild(textNode);
    newFilterBox.appendChild(btn);
  }
  //Generate selector box and components
  var selectorsBox = document.getElementById("result-filter-box");
  var newHeader = genResultSelectorHeader(filterName);
  var newFilterBox = genResultFilterBox(filterName);
  var newSelectorBox = genResultSelectorBox(
    filterName, newHeader, newFilterBox);
  selectorsBox.appendChild(newSelectorBox);

  //Generate filter buttons
  var buttonNames = RESULT_ATTRIBUTES[filterName];
  buttonNames.forEach(addButton);
  addButton("any");
}

function resultsResetFilters() {
  //Clear all filters and unfilter results
  filteredResults = resultsArray;
  filteredResults.sort();
  goToResultPage(1);
}

function goToResultPage(pageNum) {
  if (pageNum < 1) {
    //Page too low
    return;
  }
  if (pageNum > resultsLastPage()) {
    //Page too high
    return;
  }
  currentPage = pageNum;
  var pageNumDisplay = document.getElementById("results-page-number");
  pageNumDisplay.innerHTML = currentPage + "/" + resultsLastPage();
  populateResultCards((pageNum - 1) * RESULTS_PER_PAGE);
}

function resultsLastPage() {
  //Return the number of the last page
  var remainder = filteredResults.length % RESULTS_PER_PAGE;
  if (remainder > 0) {
    remainder = 1;
  }
  return Math.floor(filteredResults.length / RESULTS_PER_PAGE) + remainder;
}

function resultsPrevPage() {
  goToResultPage(currentPage - 1);
}

function resultsNextPage() {
  goToResultPage(currentPage + 1);
}

function populateResultCards(startIdx) {
  //Populate result cards from given index
  for (var i=startIdx; i<startIdx+RESULTS_PER_PAGE; i++) {
    //Iterate through results, starting at startIdx until RESULTS_PER_PAGE filled
    var cardIdx = i - startIdx;
    if (i >= filteredResults.length) {
      //Ran out of results, hide remaining cards
      hideElementById("result-card-" + cardIdx);
    } else {
      populateResultCard(cardIdx, filteredResults[i]);
      unhideElementById("result-card-" + cardIdx);
    }
  }
}

function populateResultCard(idx, resultObj) {
  //Populates a result HTML card from resultObj (JSON)
  var resultId = "result-card-" + idx;
  var name = document.getElementById(resultId + "-name");
  name.innerHTML = resultObj.name;
}
