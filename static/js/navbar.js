//Makes navigation bar responsive

function toggleSmallNavbar() {
  var navbar = document.getElementById("small-toggle-navbar");
  if (navbar.className.indexOf("w3-show") == -1) {
    //Currently hidden, unhide
    navbar.className += " w3-show";
  } else {
    //Currently shown, hide
    navbar.className = navbar.className.replace(" w3-show", "");
  }
}
