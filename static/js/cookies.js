//Handles cookie manipulation on user's side

function setCookie(cookieName, cookieValue, expireInDays) {
  //Add/change a cookie to value, expiring in N days
  var datetime = new Date();
  datetime.setTime(datetime.getTime() + (expireInDays*24*60*60*1000));
  var expires = "expires=" + datetime.toUTCString();
  var sameSite = "SameSite=Lax;";
  var path = "path=/";
  var cookie = [
    cookieName + "=" + cookieValue,
    sameSite,
    expires,
    path,
  ];
  document.cookie = cookie.join(";");
}

function deleteCookie(cookieName) {
  //Delete a cookie by setting expiry to 100 days ago
  setCookie(cookieName, "", -100);
}

function getCookieValue(cookieName) {
  //Return the value, if any, of a cookie name
  var cookie = document.cookie;
  cookie = decodeURIComponent(cookie);
  var components = cookie.split(";");
  //console.log("Cookie: " + cookie);
  //console.log("Cookie components: " + components);
  var value = null;
  for (var i=0; i<components.length; i++) {
    if (value) {
      break;
    }
    var pair = components[i].split("=");
    if (pair[0] === cookieName) {
      value = pair[1];
    }
  }
  return value;
}
