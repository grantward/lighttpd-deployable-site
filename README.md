# lighttpd-deployable-site #

Modern, responsive site quickly deploy-able via lighttpd (optionally)
in a Docker container.

## REQUIREMENTS ##
- Unix-like environment
- BASH
- npm (for compressing Javascript)

## USAGE ##

### With Docker ###

#### REQUIREMENTS ####
- Docker

First, name the docker image:
```bash
echo 'myrepo:mytag' > .docker-tag.txt
```
Build the docker image:
```bash
./scripts/docker-build.sh
```
Run the docker image:
```bash
./scripts/docker-run.sh
```

### Without Docker ###

#### REQUIREMENTS ####
- lighttpd v1
- python3 (including pypi dependencies, see requirements.txt)

```bash
./scripts/run-local.sh
```
