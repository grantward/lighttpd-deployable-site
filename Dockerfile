FROM alpine:latest

# Variables
ARG HTTPDIR=/home/http
ARG PYDIR=$HTTPDIR/py
ARG SCRIPTDIR=$HTTPDIR/scripts
ARG SRVDIR=$HTTPDIR/srv
ARG COMPRESSDIR=/tmp/lighttpdcompress

RUN apk update && \
  apk add bash && \
  apk add lighttpd && \
  apk add python3 && \
  apk add py3-pip

RUN mkdir $HTTPDIR && \
  mkdir $SCRIPTDIR && \
  mkdir $PYDIR && \
  mkdir $COMPRESSDIR

COPY ./requirements.txt $PYDIR/requirements.txt

RUN python3 -m pip install -r $PYDIR/requirements.txt

ENV PYTHONPATH "${PYTHONPATH}:$PYDIR/path"

EXPOSE 8080

CMD ["/home/http/scripts/startup.sh"]
