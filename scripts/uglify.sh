#!/bin/bash

set -euo pipefail

JSDIR="$1"
JS_IN=$(find "$JSDIR" -name '*.js' -not -path "*/ugly.js")

uglifyjs --compress --mangle -o "$JSDIR/ugly.js" -- $JS_IN
rm $JS_IN
