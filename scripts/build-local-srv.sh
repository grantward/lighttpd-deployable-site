#!/bin/bash
# Generate the files to serve via lighttpd

set -euo pipefail

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
PROJECT_DIR=$(dirname "$SCRIPT_DIR")
SRV_DIR="$PROJECT_DIR/.srv"

mkdir -p "$SRV_DIR"
mkdir -p "$SRV_DIR/webroot"
mkdir -p "$SRV_DIR/templates"
mkdir -p "$SRV_DIR/py"

cp -R "$PROJECT_DIR"/cgi-bin/* "$SRV_DIR/webroot/"
cp -R "$PROJECT_DIR/static" "$SRV_DIR/webroot/"
cp -R -T "$PROJECT_DIR/templates" "$SRV_DIR/templates"
cp -R -T "$PROJECT_DIR/pypath" "$SRV_DIR/py/path"
cp -R "$PROJECT_DIR/scripts" "$SRV_DIR/"

cp "$PROJECT_DIR/lighttpd.conf" "$SRV_DIR/"

# Compress JS into ugly.js
bash $SCRIPT_DIR/uglify.sh "$SRV_DIR/webroot/static/js"
