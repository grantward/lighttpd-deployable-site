#!/bin/bash
# Host the site in docker

set -euo pipefail

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
PROJECT_DIR=$(dirname "$SCRIPT_DIR")
SRV_DIR="$PROJECT_DIR/.srv"
CONF="$SRV_DIR/lighttpd.conf"
TARGET_DIR="/home/http"
DOCKER_TAG=$(cat "$PROJECT_DIR/.docker-tag.txt")


source "$SCRIPT_DIR/build-local-srv.sh"
printf "basedir = \"%s\"\n%s" "$TARGET_DIR" "$(cat "$CONF")" > "$CONF"

docker run --mount type=bind,src="$SRV_DIR",dst="$TARGET_DIR",readonly "$DOCKER_TAG"
