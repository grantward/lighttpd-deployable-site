#!/bin/bash
# Run the site locally without Docker

set -euo pipefail

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
PROJECT_DIR=$(dirname "$SCRIPT_DIR")
SRV_DIR="$PROJECT_DIR/.srv"
CONF="$SRV_DIR/lighttpd.conf"

source "$SCRIPT_DIR/build-local-srv.sh"
# Prepend base-dir to conf file
printf "basedir = \"%s\"\n%s" "$SRV_DIR" "$(cat "$CONF")" > "$CONF"
# Setup Python path
export PYTHONPATH="$SRV_DIR/py/path"

exec lighttpd -D -f "$PROJECT_DIR/.srv/lighttpd.conf"
