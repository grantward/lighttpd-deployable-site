#!/bin/bash

set -euo pipefail

# export PYTHONPATH="/home/py/path"

exec lighttpd -D -f "/home/http/lighttpd.conf"
