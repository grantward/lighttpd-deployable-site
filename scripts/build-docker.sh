#!/bin/bash

set -euo pipefail

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
PROJECT_DIR=$(dirname "$SCRIPT_DIR")
DOCKER_TAG_NAME=$(cat "$PROJECT_DIR/.docker-tag.txt")

docker image build -t "$DOCKER_TAG_NAME" "$PROJECT_DIR"
