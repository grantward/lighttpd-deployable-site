#!/usr/bin/env python3
"""Provides HTTP/CGI commonality."""


def add_headers(page_text, cookies=None):
    """Return the page text with HTTP headers prepended.
    Args:
        page_text (string): The HTML to render
        cookies (dict): The cookies to set in user's browser.
    Returns:
        string: The page text with headers/set-cookies
    """
    if cookies:
        cookies = '\r\n'.join([
            'Set-Cookie:{} = {};'.format(key, value)
            for key, value in cookies.items()])
    content_type = 'Content-Type: text/html'
    to_join = [content_type, '', page_text]
    if cookies:
        to_join = [cookies].extend(to_join)
    return '\r\n'.join(to_join)


if __name__ == '__main__':
    pass
