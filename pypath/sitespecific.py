#!/usr/bin/env python3
"""Provides site-specific configurations for Python CGI scripts."""

import os
import sys

from jinja2 import Environment, FileSystemLoader

SITE_SPECIFIC = os.path.abspath(__file__)
PY_PATH_DIR, _ = os.path.split(SITE_SPECIFIC)
PY_DIR, _ = os.path.split(PY_PATH_DIR)
BASE_DIR, _ = os.path.split(PY_DIR)
TEMPLATES_DIR = os.path.join(BASE_DIR, 'templates')
JINJA_DIR = os.path.join(TEMPLATES_DIR, 'jinja')

jinja_env = Environment(loader=FileSystemLoader(JINJA_DIR))


def render(template_name, *args, **kwargs):
    """Render a jinja template found in the JINJA_DIR."""
    template = jinja_env.get_template(template_name)
    return template.render(*args, **kwargs)


if __name__ == '__main__':
    pass
